package ch.inftec.ju.db;

import ch.inftec.ju.dbutil.test.jta.JtaTest;

/**
 * Helper class to run JtaTest for MySql DB.
 * @author Martin Meyer <martin.meyer@inftec.ch>
 *
 */
public class JtaTestMySql extends JtaTest {
}
