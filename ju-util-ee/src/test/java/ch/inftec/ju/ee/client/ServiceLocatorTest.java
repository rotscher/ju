package ch.inftec.ju.ee.client;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.inftec.ju.ee.client.ModifierTestProducer.TestObject;

/**
 * Run ServiceLocator tests in an embedded Weld container.
 * 
 * @author Martin Meyer <martin.meyer@inftec.ch>
 *
 */
@RunWith(Arquillian.class)
public class ServiceLocatorTest {
	@Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
            .addClass(ModifierTestProducer.class)
            .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }
	
	@Inject
	private BeanManager beanManager;
	
	@Inject
	@Named("named")
	private TestObject namedObject;
	
	@Test
	public void canInject_namedObject() {
		Assert.assertEquals("named", this.namedObject.getValue());
	}
	
	@Test
	public void canLookup_namedObject() {
		Assert.assertEquals("named", local().cdiNamed(TestObject.class, "named").getValue());
	}

	@Test
	public void canLookup_complexBeans() {
		TestObject to = local().cdiComplex(TestObject.class)
				.named("namedScope")
				.scopeControl()
				.find().one();
		
		Assert.assertEquals("namedWithScopeControl", to.getValue());
	}
	
	@Test
	public void canLookup_complexBeans_scopeControlAnnotated() {
		int cnt = local().cdiComplex(TestObject.class)
				.scopeControl()
				.find().all().size();
		
		Assert.assertEquals(2, cnt);
	}
	
	private ServiceLocator local() {
		return ServiceLocatorBuilder.createLocalByBeanManager(this.beanManager);
	}
}
