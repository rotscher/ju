JU
==

Java Utils Library - Collection of commonly used functions and libraries

Build
=====

Profiles
--------

JU contains the following profiles:

### Inherited from JB

*release-sign-artifacts*: Signs artifacts using GPG
*release-attach-javadocs*: Creates Javadoc artifacts

### Defined in JU

* *include-db-tests*: Runs DB tests on verious DB platforms
* *include-integration-tests*: Runs integration tests on a JBoss application server
* *include-fx*: Includes modules requiring JavaFX dependency
* *swisscom-release*: Can be used for version overriding when performing Swisscom Releases
* *swisscom-develop*: Can be used for Swisscom Develop build jobs. This will replace the version ID with a SNAPSHOT release

#### Activation

* By Default:
	* include-db-tests
	* 
	
* By Properties:
	* *swisscomBuildId*: Build job ID for Swisscom releases
		* swisscom-release
	* *swisscomTestBuildId*: Build job ID for Swisscom test / snapshot builds
		* swisscom-develop


* Active by default:
  * *include-db-tests*: Runs DB tests on verious DB platforms
* Activated by property _performRelease=true_ (requirements to deploy to Maven Central):
  * *release-sign-artifacts*: Signs artifacts using GPG
  * *release-attach-javadocs*: Creates Javadoc artifacts   
* Activated by property _performEswRelease=true_:
** *esw-deployment*: Deploys release to ESW Archiva repository   
* *include-fx*: Includes modules requiring JavaFX dependency
* *include-integration-tests*: Runs integration tests on a JBoss application server

Configuration Properties
------------------------

The following configuration properties can be specified using _-Dproperty=value_ to configure the build:

* Integration Tests
  * *cargo.jboss.home*: Home directory of the JBoss installation. If not specified, JBoss will be downloaded from the internet as a ZIP file
  * *ju-util-ee.portOffset*: Port offset used for the application server
  * *cargo.jboss.httpPort*: HTTP port used for pingUrl. Must comply with the port offset.

### Integration Test Profiles

Profiles automatically set properties using property files. The following profiles are available:
* *ju-ee.profile=jUnitCategoryTest*: Test the use of JUnit categories to group / exclude / include tests

### Sample executions

* Build including integration tests:
  * mvn install -P include-integration-tests -Dju-util-ee.portOffset=0 -Dju-util.profile=martinMaven
* Run single integration test:
  * mvn clean install -P include-integration-tests -Dju-util-ee.portOffset=0 -Dju-util.profile=martinMaven -rf :ju-ee-ear-ear-cargo -Dit.test=WebContainerTestIT
  
Deployment
==========

JU is set up for deployment to Sonatype (https://oss.sonatype.org). From there, it can be synched to the maven central repository
(http://search.maven.org).

* Deploy Snapshot:
  * Make sure the version is a snapshot version, e.g. 1.0-SNAPSHOT
  * run _mvn clean deploy_
* Deploy Release:
  * Make sure the version is a release version, e.g. 1.0
  * run _mvn clean deploy -DperformRelease=true_
  * Enter GPG private key password
  * Wait for build to succeed
  * Goto https://oss.sonatype.org
    * Select _Stating Repositories_ and look for a repository with profile _ch.inftec_
    * Select it, verify it and click _Close_
    * When tests are all successful, refresh and click _Release_ to release the artifacts to Maven Central repository

*Note*: In order to perform deployments, the Sonartype servers have to be configured in the settings.xml. Release artifacts
need to be signed in addition. For more info, see: https://inftec.atlassian.net/wiki/display/TEC/Maven#Maven-DeploytoCentral